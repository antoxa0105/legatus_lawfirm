## Step Project Forkio

Ссылка на [проект](https://antoxa0105.gitlab.io/step-project-forkio/#) 

#### Команднa
Igor Rakhil
Ogniev Anton

#### Использованные технологии:
   - gulp
   - gulp-sass
   - browser-sync
   - gulp-uglify
   - gulp-clean-css
   - gulp-clean
   - gulp-concat
   - gulp-imagemin
   - gulp-autoprefixer
   - postcss-uncss
 
#### Igor Rakhil #1
 - Верстка HTML, SCSS и JS для Header и секции People Are Talking About Fork;
 - Настройка рабочей среды;
 - Объединение секций Header,People Are Talking About Fork, Revolutionary Editor;
 - Код ревью

#### Ogniev Anton #2
 - Верстка блоков `Revolutionary Editor` и `Fork Subscription Pricing`.
 - Настройка рабочей среды;
 - Объединение драфт-файла и мердж с веткой Мастер;
 - Код ревью
