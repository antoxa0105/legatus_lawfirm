document.querySelector('.burger-button').addEventListener('click', function(){
    document.querySelector('.burger-button .burger-button__line').classList.toggle('js-active');
    document.querySelector('.menu').classList.toggle("js-animate");
    document.querySelector('.header__offer').classList.toggle("hidden");
});



const showChoosenPlan = (e) => {
    const currentItem = e.target.closest("div");
    if (currentItem.classList.contains('price-plan')) {
    currentItem.classList.add('price-plan--selected');
    }
}
const hideChoosenPlan = (e) => {
    const currentItem = e.target.closest("div");
    if (currentItem.classList.contains('price-plan')) {
        currentItem.classList.remove('price-plan--selected');
        }
    
}

document.querySelector(".subscription__wrapper").addEventListener("mouseover", showChoosenPlan);
document.querySelector(".subscription__wrapper").addEventListener("mouseout", hideChoosenPlan);